/*
macli - Unofficial CLI-Based MyAnimeList Client
Copyright © 2022-2024 Vidhu Kant Sharma <vidhukant@vidhukant.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public Lice
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package auth

var HTMLTemplate = `
<html>
  <head>
    <title>macli - Unofficial MyAnimeList Client</title>
  </head>
  <body>
    <style>
    body {
      background-color: #232627;
      display: flex;
      flex-direction: column;
      justify-content: center;
      gap: 0.5rem;
      align-items: center;
      min-height: 100vh;
    }
    a {
      color: #C678DD;
    }
    #heading, #subheading, #description {
      text-align: center;
      margin: 0;
    }
    #heading {
      color: #C678DD;
      font-size: 2.2em;
    }
    #subheading {
      color: #dfdfdf;
      font-size: 1.2em;
    }
    #description {
      margin-top: 2rem;
      color: lightgray;
      font-size: 1em;
    }
    </style>
    <p id="heading">%s</p>
    <p id="subheading">%s</p>
    <p id="description"><a href="https://macli.vidhukant.com">https://macli.vidhukant.com</a></p>
  </body>
</html>
`
